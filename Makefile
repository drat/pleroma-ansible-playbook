VAPID_PRIVATE_KEY_FILE := vapid-private-key.pem
INVENTORY ?= "hosts.yml"
PLAYBOOK_TAGS ?= "all"
PLAYBOOK_FILE ?= "playbook.yml"
DOCKERFILE ?= Dockerfile
DOCKER_CONTEXT ?= .
IMAGE_NAME ?= pleroma-ansible-playbook
IMAGE_TAG ?= latest
IMAGE_DATE ?= nil
DOCKER_IMAGE = $(IMAGE_NAME):$(IMAGE_TAG)

PHONY: secret_key_base signing_salt vapid_private_key vapid_public_key test_modules_unit image publish pleroma

all: secret_key_base signing_salt vapid_key_pair

vapid_key_pair: vapid_private_key vapid_public_key

secret_key_base:
	@echo "Secret key base:"
	@openssl rand -base64 48

signing_salt:
	@echo -e "\nSigning salt:"
	@openssl rand -hex 4

$(VAPID_PRIVATE_KEY_FILE):
	@openssl ecparam -genkey -name prime256v1 -out $(VAPID_PRIVATE_KEY_FILE)

vapid_private_key: $(VAPID_PRIVATE_KEY_FILE)
	@echo -e "\nVapid private key:"
	@openssl ec -in $(VAPID_PRIVATE_KEY_FILE) -outform DER 2> /dev/null | tail -c +8 | head -c 32 | base64 | tr '/+' '_-' | tr -d '=' | tr -d '\n'

vapid_public_key: $(VAPID_PRIVATE_KEY_FILE)
	@echo -e "\n\nVapid public key:"
	@openssl ec -in $(VAPID_PRIVATE_KEY_FILE) -pubout -outform DER 2> /dev/null | tail -c 65 | base64 | tr '/+' '_-' | tr -d '=' | tr -d '\n'

test_modules_unit:
	@find ./library -mindepth 1 -maxdepth 1 -type f -name test_*.py | xargs python3

image:
	@docker build $(BUILD_ARGS) -f $(DOCKERFILE) -t $(DOCKER_IMAGE) $(DOCKER_CONTEXT)

publish: image
	@docker push $(DOCKER_IMAGE)

pleroma:
	ansible-playbook --inventory $(INVENTORY) --tags $(PLAYBOOK_TAGS) $(EXTRA_ARGS) $(PLAYBOOK_FILE)
