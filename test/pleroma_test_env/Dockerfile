FROM dockage/alpine:3.10-openrc

ARG TEST_ENV_UID=1100
ARG TEST_ENV_USER=admin
ARG TEST_ENV_PASSWORD
ARG SSH_DIR=/home/admin/.ssh
SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

RUN apk add --no-cache \
        bash=5.0.0-r0 \
        openssh=8.1_p1-r0 \
        sudo=1.8.27-r2 \
        python3=3.7.7-r0 \
    && \
    adduser -u ${TEST_ENV_UID} -s /bin/bash -D ${TEST_ENV_USER} && \
    echo ${TEST_ENV_USER}:${TEST_ENV_PASSWORD} | chpasswd && \
    mkdir ${SSH_DIR} && \
    chown ${TEST_ENV_USER}:${TEST_ENV_USER} ${SSH_DIR} && chmod 0700 ${SSH_DIR} && \
    touch ${SSH_DIR}/authorized_keys && \
    chown ${TEST_ENV_USER}:${TEST_ENV_USER} ${SSH_DIR}/authorized_keys && \
    chmod 0600 ${SSH_DIR}/authorized_keys && \
    rc-status && \
    touch /run/openrc/softlevel

COPY files/entrypoint /entrypoint
COPY files/admin-sudoers /etc/sudoers.d/admin

EXPOSE 22 80 443

CMD ["/entrypoint"]
