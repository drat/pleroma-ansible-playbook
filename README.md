# Ansible Playbook for Pleroma

## Table of content

- [Summary](#summary)
- [Requirements](#requirements)
- [Configuration](#configuration)
- [Secrets](#secrets)
- [Guide to setting up and running the playbook](#guide-to-setting-up-and-running-the-playbook)

## Summary

This project is a configurable playbook that can install, configure and customise Pleroma on an Alpine Linux host.
The playbook is currently used to manage my personal instance at https://fedi.dananglin.me.uk.
It currently only supports installing Pleroma on a single host but will support installing it across multipe hosts in the future.

This project was inspired by the official [Pleroma OTP installation guide](https://docs.pleroma.social/otp_en.html#content).

This project is developed over at https://gitlab.com/dananglin/pleroma-ansible-playbook. The master branch is mirrored over at https://github.com/dananglin/pleroma-ansible-playbook.

The project's icon ([Books icon](https://icons8.com/icon/114010/books)) was downloaded from [Icons8](https://icons8.com).

### Ansible roles

There are four roles used to install and configure your Pleroma instance:

- **init:** merges the default configuration with your custom configuration.
- **pleroma-database:** maintains the database layer by installing and configuring the PostgreSQL database server, creating the database user and creating and administrating the Pleroma database.
- **pleroma-main:** maintains the main layer by handling the Pleroma installation, Pleroma upgrades and the configuration of the Pleroma frontend and backend.
- **pleroma-proxy:** maintains the proxy layer by installing and configuring Nginx, creating the TLS certificates using Let's Encrypt, adding support for proving your Pleroma site with Keybase, etc.

### Additional features

- **Let's Encrypt support:** This playbook creates a TLS certificate using Let's Encrypt.
- **Keybase support:** Pleroma does not support Keybase out of the box but you can still prove that your ownership of your Pleroma site.
- **Set default background:** You can specify an image to use as the default background of your Pleroma site.
- **Upload custom themes:** You can upload custom Pleroma themes in to your Pleroma instance.
- **Set default theme:** You can specify the default Pleroma theme.

## Requirements

### For your controller host

- The controller host should the following packages installed:
  - [Ansible](https://www.ansible.com/) version 2.8+.
  - make
  - openssl

### For your target Pleroma host

- The target host should be running [Alpine Linux](https://www.alpinelinux.org/) version 3.10+.
- A (sub)domain which resolves to the IP address of the target host.
- A user with sudo privileges (optional, but preferable).
- The following packages installed:
  - sudo
  - python (version 3.5 or higher)

## Configuration

Here's an [example configuration file](./examples/host_vars/pleroma-01.yml) that you can use as a starting point to configure your Pleroma instance.
This typically goes in your host\_vars directory but you can place it in your group\_vars directory or even inside your playbook instead.
The [default configuration is located here](./roles/init/defaults/main.yml) which the `init` role will merge with your configuration.
Any fields you configured will overwrite the default.

More documentation on the configuration will be available soon.

## Secrets

Following secrets are not included in the default configuration and must be generated before running the playbook:

- **secretKeyBase:** This is used to configure the `secret_key_base` in Pleroma. This is used to sign and verify cookies.
- **signingSalt:** This is used to configure the `signing_salt` in Pleroma. This is used with the `secret_key_base` to generate a key for signing and verifying cookies.
- **vapid key pair for web push encryption:** This is a private and public key pair so that Pleroma can used [VAPID](https://tools.ietf.org/html/rfc8292) to identify itself to the web push service (for notifications in the browser).
- **database password:** This is used to authenticate access to the Pleroma database.

Instructions on generating these can be found in the guide below.
It is recommended to encrypt these secrets using [Ansible Vault](https://docs.ansible.com/ansible/latest/user_guide/vault.html).

## Guide to setting up and running the playbook

- Copy the [example inventory file](examples/inventory.yml) to the root of this project.
  ```bash
  $ cp examples/inventory.yml ./
  ```

- In the inventory file you've just copied change **\<ANSIBLE\_HOSTS\>** to the IP address of the target host and change **\<ANSIBLE\_USER\>** to the user on the target host with sudo privileges.

- Copy the [example playbook file](examples/site.yml) to the root of the project.
  ```bash
  $ cp examples/site.yml ./
  ```

- Copy the [example host\_vars](examples/host_vars) directory to the root of this project. This directory contains the file used to configure your Pleroma instance. You should review and edit the configuration before running the playbook.
  ```bash
  $ cp -a examples/host_vars ./
  ```

- Generate the secret key base and add this to the to the `secretKeyBase` field (don't forget to uncomment this).
  ```bash
  $ make secret_key_base
  ```

- Generate the signing salt and add this to the `signingSalt` field.
  ```
  $ make signing_salt
  ```

- Generate the key pair for web push encryption and add these to `privateKey` and `publicKey` fields.
  ```
  $ make vapid_key_pair
  ```

- Create a password for your database and add this to the `password` field under `db`.

- Optional (but recommended): Use Ansible Vault to encrypt the generated secret values above.

- Run the playbook using one of the following commands:
  ```bash
  # If you're not using Ansible vault
  $ ansible-playbook -i inventory.yml site.yml

  # If you're using Ansible vault and want to be prompted for the password
  $ ansible-playbook -i inventory.yml site.yml --ask-vault-pass

  # If you're using Ansible vault and a password file
  $ ansible-playbook -i inventory.yml site.yml --vault-id /path/to/your/password-file
  ```
