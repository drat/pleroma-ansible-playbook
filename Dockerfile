FROM alpine:3.11

ARG ANSIBLE_UID=1200
ARG ANSIBLE_USER=ansible
ARG ANSIBLE_VERSION=2.9.7
ARG PLEROMA_PLAYBOOK_DIRECTORY=/ansible/pleroma-playbook
ARG BUILD_TIME
ARG BUILD_TAG

# Annotation (label) schema based on the OCI image specification.
# https://github.com/opencontainers/image-spec/blob/master/annotations.md
LABEL org.opencontainers.image.authors="Dan Anglin <d.n.i.anglin@gmail.com>" \
      org.opencontainers.image.created=${BUILD_TIME} \
      org.opencontainers.image.documentation="https://gitlab.com/dananglin/pleroma-ansible-playbook/-/blob/master/README.md" \
      org.opencontainers.image.source="https://gitlab.com/dananglin/pleroma-ansible-playbook.git" \
      org.opencontainers.image.version=${BUILD_TAG} \
      org.opencontainers.image.vendor="Dan Anglin" \
      org.opencontainers.image.licenses="MIT" \
      org.opencontainers.image.title="Pleroma Ansible Playbook" \
      org.opencontainers.image.description="Ansible playbook that installs, configures and customizes Pleroma on a Alpine host."

# TODO: Remove sshpass when it is possible to do so.
RUN \
    apk add --no-cache \
      ca-certificates=20191127-r2 \
      make=4.2.1-r2 \
      openssh-client=8.1_p1-r0 \
      openssl=1.1.1g-r0 \
      python3=3.8.2-r0 \
      bash=5.0.11-r1 \
      sshpass=1.06-r0 \
    && \
    apk add --no-cache --virtual .build-deps \
      python3-dev=3.8.2-r0 \
      libffi-dev=3.2.1-r6 \
      openssl-dev=1.1.1g-r0 \
      build-base=0.5-r1 \
    && \
    pip3 install --upgrade \
      pip==20.0.2 \
      cffi==1.14.0 \
    && \
    pip install \
      ansible==${ANSIBLE_VERSION} \
      hvac==0.10.3 \
    && \
    apk del \
      .build-deps \
    && \
    adduser -u ${ANSIBLE_UID} -s /bin/sh -D ${ANSIBLE_USER}

COPY --chown=${ANSIBLE_UID}:${ANSIBLE_UID} library ${PLEROMA_PLAYBOOK_DIRECTORY}/library/
COPY --chown=${ANSIBLE_UID}:${ANSIBLE_UID} roles ${PLEROMA_PLAYBOOK_DIRECTORY}/roles/
COPY --chown=${ANSIBLE_UID}:${ANSIBLE_UID} playbook.yml ${PLEROMA_PLAYBOOK_DIRECTORY}/playbook.yml
COPY --chown=${ANSIBLE_UID}:${ANSIBLE_UID} Makefile ${PLEROMA_PLAYBOOK_DIRECTORY}/Makefile

ENV ANSIBLE_HOST_KEY_CHECKING=False \
    ANSIBLE_PYTHON_INTERPRETER=/usr/bin/python3 \
    ANSIBLE_SSH_PIPELINING=False \
    ANSIBLE_GATHERING=smart \
    ANSIBLE_RETRY_FILES_ENABLED=False \
    ANSIBLE_PERSISTENT_CONNECT_TIMEOUT=30 \
    ANSIBLE_PERSISTENT_COMMAND_TIMEOUT=60

USER ${ANSIBLE_USER}
WORKDIR ${PLEROMA_PLAYBOOK_DIRECTORY}
CMD [ "make", "pleroma" ]
