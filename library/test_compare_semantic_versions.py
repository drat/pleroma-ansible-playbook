#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright: (c) 2020, Daniel Anglin <d.n.i.anglin@gmail.com>
# The MIT License (see LICENSE or https://mit-license.org)

import unittest as u
import compare_semantic_versions as c

class TestCompareSemanticVersions(u.TestCase):
    def test_compare_versions_results(self):
        self.assertEqual(c.compare_versions("2.3.0", "3.0.0"), c.UPGRADE)
        self.assertEqual(c.compare_versions("2.4.0", "2.3.0"), c.DOWNGRADE)
        self.assertEqual(c.compare_versions("1.2.2", "1.2.3"), c.UPGRADE)
        self.assertEqual(c.compare_versions("v2", "2.0.0"), c.NO_VERSION_CHANGE)
        self.assertEqual(c.compare_versions("1.4.0", "v1.4"), c.NO_VERSION_CHANGE)
        self.assertEqual(c.compare_versions("12.24.1", "v12.25.6"), c.UPGRADE)

    def test_compare_versions_exceptions(self):
        with self.assertRaises(ValueError) as ctx:
            c.compare_versions("1.2.3.4", "1.2.3")
        self.assertEqual("invalid number of version components in semantic version '1.2.3.4'.", str(ctx.exception))

        with self.assertRaises(ValueError) as ctx:
            c.compare_versions("10.3.1", "11..3")
        self.assertRegex(str(ctx.exception), r'^unable to parse an int for semantic version \'11..3\' due to invalid format\..*$')

        with self.assertRaises(ValueError) as ctx:
            c.compare_versions("10.FOUR.8", "10.4.8")
        self.assertRegex(str(ctx.exception), r'^unable to parse an int for semantic version \'10.FOUR.8\' due to invalid format\..*$')

if __name__ == '__main__':
    u.main()
