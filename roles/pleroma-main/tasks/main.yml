---
- name: Ensure Pleroma dependencies are installed.
  apk:
    name: unzip, ncurses
    state: present

- name: Ensure group '{{ pleroma_user.group }}' exists.
  group:
    gid: "{{ pleroma_user.id }}"
    name: "{{ pleroma_user.group }}"
    state: present
    system: yes

- name: Ensure user '{{ pleroma_user.name }}' exists.
  user:
    group: "{{ pleroma_user.group }}"
    home: "{{ pleroma_user.home }}"
    name: "{{ pleroma_user.name }}"
    shell: "{{ pleroma_user.shell }}"
    state: present
    system: yes
    uid: "{{ pleroma_user.id }}"

- name: Ensure the Pleroma data and config directories exists.
  file:
    name: "{{ item }}"
    state: directory
    mode: '0700'
    owner: "{{ pleroma_user.name }}"
    group: "{{ pleroma_user.group }}"
  loop:
  - "{{ pleroma_config_dir }}"
  - "{{ pleroma_base_data_dir }}"
  - "{{ pleroma_uploads_dir }}"
  - "{{ pleroma_static_dir }}"
  - "{{ pleroma_static_dir }}/images"
  - "{{ pleroma_static_dir }}/static"
  - "{{ pleroma_static_dir }}/static/themes"

- name: Checking if Pleroma is already installed.
  stat:
    path: "{{ pleroma_user.home }}/bin/pleroma"
  register: pleroma_bin

- debug:
    msg: "Pleroma is currently installed."
    verbosity: 0
  when: pleroma_bin.stat.isreg is defined

- debug:
    msg: "Pleroma does not appear to be installed. It will be downloaded and installed."
    verbosity: 0
  when: pleroma_bin.stat.isreg is not defined

- name: Registering the 'enable_pleroma_download' flag.
  set_fact:
    enable_pleroma_download: true
  when: (pleroma_bin.stat.isreg is not defined) or
        (enable_pleroma_upgrade | default(False))

- name: Registering the 'enable_pleroma_installation' flag.
  set_fact:
    enable_pleroma_installation: true
  when: pleroma_bin.stat.isreg is not defined

- name: Ensuring that the stable release build of pleroma is downloaded.
  get_url:
    url: "{{ pleroma_download_url }}"
    dest: "{{ pleroma_download_dest }}"
  when: enable_pleroma_download | default(False)

- name: Unzipping the release build of pleroma.
  unarchive:
    remote_src: yes
    src: "{{ pleroma_download_dest }}"
    dest: /tmp
    owner: "{{ pleroma_user.name }}"
    group: "{{ pleroma_user.group }}"
  when: enable_pleroma_download | default(False)

- name: Registering the installed version of Pleroma.
  shell: "{{ pleroma_user.home }}/bin/pleroma version | awk '{print $2}' | awk -F - '{print $1}'"
  register: pleroma_installed_version
  when: enable_pleroma_upgrade | default(False)

- debug:
    msg: "Version {{ pleroma_installed_version.stdout }} is installed."
    verbosity: 0
  when: enable_pleroma_upgrade | default(False)

- name: Registering the downloaded version of Pleroma.
  shell: "/tmp/release/bin/pleroma version | awk '{print $2}' | awk -F - '{print $1}'"
  register: pleroma_downloaded_version
  when: enable_pleroma_upgrade | default(False)

- debug:
    msg: "Version {{ pleroma_downloaded_version.stdout }} is downloaded."
    verbosity: 0
  when: enable_pleroma_upgrade | default(False)

- name: Comparing the installed and downloaded versions of Pleroma.
  compare_semantic_versions:
    old_version: "{{ pleroma_installed_version.stdout }}"
    new_version: "{{ pleroma_downloaded_version.stdout }}"
  register: comparison
  when: enable_pleroma_upgrade | default(False)

- fail:
    msg: "This playbook does not currently support downgrading Pleroma."
  when: comparison.result is defined and comparison.result == "downgrade"

- debug:
    msg: "Pleroma is already installed at the target version {{ pleroma_downloaded_version.stdout }}."
    verbosity: 0
  when: comparison.result is defined and comparison.result == "noVersionChange"

- debug:
    msg: "Pleroma will be upgraded to version {{ pleroma_downloaded_version.stdout }}."
    verbosity: 0
  when: comparison.result is defined and comparison.result == "upgrade"

- name: Registering the 'enable_pleroma_installation' flag for the upgrade.
  set_fact:
    enable_pleroma_installation: true
  when: comparison.result is defined and comparison.result == "upgrade"

- name: Ensuring that the Pleroma service is stopped.
  service:
    name: pleroma
    state: stopped
  when: comparison.result is defined and comparison.result == "upgrade"

- name: Ensuring that the previous version of Pleroma is uninstalled.
  shell: |
    find {{ pleroma_user.home }} -mindepth 1 -maxdepth 1 | xargs -I dir rm -rf dir
  when: comparison.result is defined and comparison.result == "upgrade"

- name: Ensuring that Pleroma is installed.
  shell: |
    find /tmp/release/ -mindepth 1 -maxdepth 1 | xargs -I dir mv dir {{ pleroma_user.home }}
  args:
    creates: "{{ pleroma_user.home }}/bin/pleroma"
  notify: "restart pleroma"
  when: enable_pleroma_installation is defined

- name: Ensuring the configuration file is set.
  template:
    src: etc_pleroma_config.exs.j2
    dest: "{{ pleroma_config_dir }}/config.exs"
    owner: "{{ pleroma_user.name }}"
    group: "{{ pleroma_user.group }}"
    mode: '0400'
  notify: "restart pleroma"

- name: Running database migrations.
  command:
    argv:
    - pleroma_ctl
    - migrate
  environment:
    PATH: "{{ ansible_env.PATH }}:/opt/pleroma/bin"
  when: enable_pleroma_installation is defined

- name: Ensuring that folder permissions are set properly in /opt/pleroma.
  shell: |
    find {{ pleroma_user.home }} -mindepth 1 -maxdepth 1 -type d | xargs -I dir chmod 0700 dir
    find {{ pleroma_user.home }} -mindepth 1 -maxdepth 1 -type d | xargs -I dir chown -R {{ pleroma_user.name }}:{{ pleroma_user.group }} dir

- name: Uploading the background image to the static directory
  copy:
    src: "{{ pleroma.config.frontend.background }}"
    dest: "{{ pleroma_static_dir }}/images/pleroma-background.png"
    owner: "{{ pleroma_user.name }}"
    group: "{{ pleroma_user.group }}"
    mode: '0400'
  notify: "restart pleroma"
  when: pleroma.config.frontend.background|length

- name: Ensuring that the custom themes are uploaded.
  copy:
    src: "{{ item.path }}"
    dest: "{{ pleroma_static_dir }}/static/themes/{{ item.name }}.json"
    owner: "{{ pleroma_user.name }}"
    group: "{{ pleroma_user.group }}"
    mode: '0400'
  loop: "{{ pleroma.config.frontend.themes.custom }}"

- name: Ensuring the styles.json file is set in the static/static directory.
  template:
    src: var_lib_pleroma_static_static_styles.json.j2
    dest: "{{ pleroma_static_dir }}/static/styles.json"
    owner: "{{ pleroma_user.name }}"
    group: "{{ pleroma_user.group }}"
    mode: '0400'

- name: Ensuring that the pleroma init file is installed.
  copy:
    src: "{{ pleroma_user.home }}/installation/init.d/pleroma"
    dest: /etc/init.d/pleroma
    remote_src: yes
    owner: root
    group: root
    mode: '0700'
  notify: "restart pleroma"

- name: Ensuring that the Pleroma service is enabled.
  service:
    name: pleroma
    enabled: yes

- name: Cleaning up
  file:
    path: "{{ item }}"
    state: absent
  loop:
  - "{{ pleroma_download_dest }}"
  - /tmp/release
